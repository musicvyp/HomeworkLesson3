/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

// Вариант на уровне моего понимания

// var images = document.getElementsByTagName('img');
// var allImg = ["images/cat1.jpg", "images/cat2.jpg", "images/cat3.jpg", "images/cat4.jpg",
//     "images/cat5.jpg", "images/cat6.jpg", "images/cat7.jpg", "images/cat8.jpg"];
// var n = 0;

// console.log(allImg);

// var pic1 = document.createElement("img");
// slider.appendChild(pic1);

// window.onload = function() {
//     pic1.src = allImg[n];
// }

// function next() {
//         if (n <= 6) {
//             n++;
//             pic1.src = allImg[n]
//             console.log(n);
//         } else {
//             n = 0;
//             pic1.src = allImg[n];
//         }

//     }
// function prev() {
//         if (n > 0) {
//             n--;
//             pic1.src = allImg[n]
//             console.log(n);
//         } else {
//             n = 7;
//             pic1.src = allImg[n];
//         }
//     }

// PrevSilde.addEventListener( "click", prev);
// NextSilde.addEventListener( "click", next);


// Вариант второй более универсальный, но ненамного. 

var pic1 = document.createElement("img");
var images = document.getElementsByTagName('img');

slider.appendChild(pic1);
pic1.style.height = "50px";
var n = 1;

window.onload = function() {
    
    pic1.style.transform = "rotate(720deg)";
    pic1.style.height = "512px";
    pic1.style.transition= "all 2s ease-out";

    pic1.src = "images/cat" + n + ".jpg";

}
window.addEventListener('click', onload);

function next() {
        if (n <= 7) {
            n++;
            onload;
            console.log(n);
        } else {
            n = 1;
           onload;
        }

    }
function prev() {
        if (n > 1) {
            n--;
            onload;
            console.log(n);
        } else {
            n = 8;
            onload;
        }
    }

PrevSilde.addEventListener( "click", prev);
NextSilde.addEventListener( "click", next);

